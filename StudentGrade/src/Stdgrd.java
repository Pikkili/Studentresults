
import java.util.Random;
public class Stdgrd {
	public static void main (String args[])
	{
		int sci,mat,soc,eng,tel,hin;
		int tot,avg,i;
		boolean pass = false;
		sci=mat=soc=eng=tel=hin=avg=0;
		Random randomGenerator = new Random();
		for(i=0;i<6;i++)
		{
			if(i==0) sci = randomGenerator.nextInt(100);
			if(i==1) mat = randomGenerator.nextInt(100);
			if(i==2) eng = randomGenerator.nextInt(100);
			if(i==3) tel = randomGenerator.nextInt(100);
			if(i==4) hin = randomGenerator.nextInt(100);
			if(i==5) soc = randomGenerator.nextInt(100);
		}
		tot = sci+mat+eng+tel+hin+soc;
		if(sci >= 35 && mat >= 35 && eng >= 35 && tel >=35 && hin >=35 && soc >= 35)
		{
			avg = tot/6;
			pass = true;
		}
		
		if(avg >= 70 && avg < 100 && pass)
		{
			System.out.println("**************************************************************\n");
			System.out.println("Congratulations!!!");
			System.out.println("Distinction");
			System.out.println("\n**************************************************************\n");
			System.out.println("Here are you marks");
			System.out.println("Subject\t\tMarks");
			System.out.println("----------------------------");
			System.out.println("English : \t"+eng);
			System.out.println("Telugu  : \t"+tel);
			System.out.println("Hindi   : \t"+hin);
			System.out.println("Science : \t"+sci);
			System.out.println("Maths   : \t"+mat);
			System.out.println("Social  : \t"+soc);
		}
		else if(avg >=60 && avg < 70 && pass)
		{
			System.out.println("**************************************************************\n");
			System.out.println("Congratulations!!!");
			System.out.println("First class");
			System.out.println("\n**************************************************************\n");
			System.out.println("Here are you marks");
			System.out.println("Subject\t\tMarks");
			System.out.println("----------------------------");
			System.out.println("English : \t"+eng);
			System.out.println("Telugu  : \t"+tel);
			System.out.println("Hindi   : \t"+hin);
			System.out.println("Science : \t"+sci);
			System.out.println("Maths   : \t"+mat);
			System.out.println("Social  : \t"+soc);
		}
		else if(avg >= 50 && avg < 60 && pass)
		{
			System.out.println("**************************************************************\n");
			System.out.println("Congratulations!!!");
			System.out.println("Second class");
			System.out.println("\n**************************************************************\n");
			System.out.println("Here are you marks");
			System.out.println("Subject\t\tMarks");
			System.out.println("----------------------------");
			System.out.println("English : \t"+eng);
			System.out.println("Telugu  : \t"+tel);
			System.out.println("Hindi   : \t"+hin);
			System.out.println("Science : \t"+sci);
			System.out.println("Maths   : \t"+mat);
			System.out.println("Social  : \t"+soc);
		}
		else if(avg >= 35 && avg < 50 && pass)
		{
			System.out.println("**************************************************************\n");
			System.out.println("Congratulations!!!");
			System.out.println("Third class");
			System.out.println("\n**************************************************************\n");
			System.out.println("Here are you marks");
			System.out.println("Subject\t\tMarks");
			System.out.println("----------------------------");
			System.out.println("English : \t"+eng);
			System.out.println("Telugu  : \t"+tel);
			System.out.println("Hindi   : \t"+hin);
			System.out.println("Science : \t"+sci);
			System.out.println("Maths   : \t"+mat);
			System.out.println("Social  : \t"+soc);
		}
		else
		{
			System.out.println("**************************************************************\n");
			System.out.println("Sorry!!!");
			System.out.println("Fail");
			System.out.println("\n**************************************************************\n");
			System.out.println("Here are you marks");
			System.out.println("Subject\t\tMarks");
			System.out.println("----------------------------");
			System.out.println("English : \t"+eng);
			System.out.println("Telugu  : \t"+tel);
			System.out.println("Hindi   : \t"+hin);
			System.out.println("Science : \t"+sci);
			System.out.println("Maths   : \t"+mat);
			System.out.println("Social  : \t"+soc);
		}
		
	}

}
